﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using NUnit;
using NUnit.Framework;

namespace FileAnalyzer.Tests
{
    [TestFixture]
    public class Program
    {
        #region Methods

        [Test]
        public void GetFirstNameFrequencyResultsTest()
        {
            var results = FileAnalyzer.Program.GetFirstNameFrequencyResults(GetRecords());
            var result = results.FirstOrDefault();

            Assert.NotNull(result);
            Assert.AreEqual(result.Name, "John");
            Assert.AreEqual(result.Frequency, 2);
        }

        [Test]
        public void GetSurnameFrequencyResultsTest()
        {
            var results = FileAnalyzer.Program.GetSurnameFrequencyResults(GetRecords());
            var result = results.FirstOrDefault();

            Assert.NotNull(result);
            Assert.AreEqual(result.Name, "Smith");
            Assert.AreEqual(result.Frequency, 2);
        }

        [Test]
        public void GetNameResultsTest()
        {
            var results = FileAnalyzer.Program.GetNameResults(GetRecords());
            var result = results.FirstOrDefault();

            Assert.NotNull(result);
            Assert.AreEqual(result.Surname, "Jones");
        }

        [Test]
        public void GetAddressResultsTest()
        {
            var results = FileAnalyzer.Program.GetAddressResults(GetRecords());
            var result = results.FirstOrDefault();

            Assert.NotNull(result);
            Assert.AreEqual(result.Street, "ABC Street");
        }

        private IEnumerable<Record> GetRecords()
        {
            List<Record> records = new List<Record>();

            Record record1 = new Record()
            {
                FirstName = "John",
                Surname = "Smith",
                StreetNumber = 1,
                Street = "ABC Street"
            };

            records.Add(record1);

            Record record2 = new Record()
            {
                FirstName = "Amanda",
                Surname = "Jones",
                StreetNumber = 2,
                Street = "DEF Street"
            };

            records.Add(record2);

            Record record3 = new Record()
            {
                FirstName = "Tim",
                Surname = "Matthews",
                StreetNumber = 3,
                Street = "GHI Street"
            };

            records.Add(record3);

            Record record4 = new Record()
            {
                FirstName = "John",
                Surname = "Purcell",
                StreetNumber = 4,
                Street = "JKL Street"
            };

            records.Add(record4);

            Record record5 = new Record()
            {
                FirstName = "Adam",
                Surname = "Smith",
                StreetNumber = 5,
                Street = "MNO Street"
            };

            records.Add(record5);

            return records;
        }
        
        #endregion
    }
}
