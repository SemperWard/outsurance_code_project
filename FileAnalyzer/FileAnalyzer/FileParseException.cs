﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace FileAnalyzer
{
    public class FileParseException : Exception
    {
        #region Constructors

        public FileParseException()
            : base()
        {
            
        }

        public FileParseException(string message)
            : base(message)
        {
        }


        public FileParseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected FileParseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
    }
}
