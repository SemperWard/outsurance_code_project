﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAnalyzer
{
    public class NameFrequencyResult
    {
        #region Constructors

        public NameFrequencyResult()
        {
        }

        #endregion

        #region Properties

        public string Name { get; set; }
        public  int Frequency { get; set; }

        #endregion
    }
}
