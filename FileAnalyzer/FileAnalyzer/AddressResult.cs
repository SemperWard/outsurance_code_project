﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAnalyzer
{
    public class AddressResult
    {
        #region Constructors

        #endregion

        #region  Properties

        public  uint StreetNumber { get; set; }
        public  string Street { get; set; }

        #endregion
    }
}
