﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace FileAnalyzer
{
    [DelimitedRecord(",")]
    public class Record
    {
        #region Constructors

        public Record()
        {
        }

        #endregion

        #region Fields

        public string FirstName;
        public string Surname;
        [FieldConverter(ConverterKind.UInt32)]
        public uint StreetNumber;
        public string Street;

        #endregion
    }
}
