﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace FileAnalyzer
{
    public class PrintException : Exception
    {
        #region Constructors

        public PrintException()
            : base()
        {
            
        }

        public PrintException(string message)
            : base(message)
        {
            
        }

        public PrintException(string message, Exception innerException)
            : base(message, innerException)
        {
            
        }

        protected PrintException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            
        }

        #endregion
    }
}
