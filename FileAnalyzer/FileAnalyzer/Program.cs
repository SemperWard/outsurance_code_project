﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using FileHelpers;

namespace FileAnalyzer
{
    public class Program
    {
        #region Fields

        #endregion

        #region  Methods

        public static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                string filePath = args[0];
                string outputPath = args[1];

                if (!File.Exists(filePath))
                {
                    Console.WriteLine("Missing or invalid input file path.");
                    Console.WriteLine();
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey(true);
                    return;
                }
                else if (! Directory.Exists(outputPath))
                {
                    Console.WriteLine("Missing or invalid output path.");
                    Console.WriteLine();
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey(true);
                    return;
                }

                try
                {
                    var records = ParseFile(filePath);

                    var firstNameResults = GetFirstNameFrequencyResults(records);
                    var surnameResults = GetSurnameFrequencyResults(records);
                    var nameResults = GetNameResults(records);
                    PrintNameResults(outputPath, firstNameResults, surnameResults, nameResults);

                    var addressResults = GetAddressResults(records);
                    PrintAddressResults(outputPath, addressResults);
                }
                catch (FileParseException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (PrintException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unknown error occurred. {0}", ex.ToString());
                }

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey(true);
            }
            else
            {
                DisplayHelp();
            }
        }

        public static void DisplayHelp()
        {
            Console.WriteLine("FileAnalyzer usage:");
            Console.WriteLine();
            Console.WriteLine("FileAnalyzer.exe <filePath> <outputPath>");
            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(true);
        }

        public static IEnumerable<Record> ParseFile(string filePath)
        {
            Record[] records = null;
            var engine = new FileHelperEngine<Record>();

            try
            {
                records = engine.ReadFile(filePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new FileParseException("Error parsing file, " + filePath, ex);
            }

            return records;
        }

        public  static IEnumerable<NameFrequencyResult> GetFirstNameFrequencyResults(IEnumerable<Record> records)
        {
            var results = from r in records
                            group r by r.FirstName
                    into g
                    orderby g.Count() descending 
                            select new NameFrequencyResult() {Name = g.Key, Frequency = g.Count()};

            return results;
        }

        public static IEnumerable<NameFrequencyResult> GetSurnameFrequencyResults(IEnumerable<Record> records)
        {
            var results = from r in records
                          group r by r.Surname
                into g
                          orderby g.Count() descending
                          select new NameFrequencyResult() { Name = g.Key, Frequency = g.Count() };

            return results;
        }

        public static IEnumerable<NameResult> GetNameResults(IEnumerable<Record> records)
        {
            var results = from r in records
                orderby r.Surname ascending
                select new NameResult() {FirstName = r.FirstName, Surname = r.Surname};

            return results;
        }

        public static IEnumerable<AddressResult> GetAddressResults(IEnumerable<Record> records)
        {
            var results = from r in records
                orderby r.Street ascending
                select new AddressResult() {StreetNumber = r.StreetNumber, Street = r.Street};

            return results;
        }

        public static void PrintNameResults(string outputPath, IEnumerable<NameFrequencyResult> firstNameFrequencyResults, IEnumerable<NameFrequencyResult> surnameFrequencyResults, IEnumerable<NameResult> nameResults  )
        {
            string filePath = string.Format("{0}\\{1}", outputPath, "NameResults.txt");
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(filePath);

                foreach (var result in firstNameFrequencyResults)
                {
                    sw.WriteLine("{0}, {1}", result.Name, result.Frequency);
                }

                sw.WriteLine();

                foreach (var result in surnameFrequencyResults)
                {
                    sw.WriteLine("{0}, {1}", result.Name, result.Frequency);
                }

                sw.WriteLine();

                foreach (var result in nameResults)
                {
                    sw.WriteLine("{0} {1}", result.FirstName, result.Surname);
                }
            }
            catch (Exception ex)
            {
                throw new PrintException("Error printing results.", ex);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        public static void PrintAddressResults(string outputPath, IEnumerable<AddressResult> addressResults)
        {
            string filePath = string.Format("{0}\\{1}", outputPath, "AddressResults.txt");
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(filePath);

                foreach (var result in addressResults)
                {
                    sw.WriteLine("{0} {1}", result.StreetNumber, result.Street);
                }
            }
            catch (Exception ex)
            {
                throw new PrintException("Error printing results.", ex);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        #endregion
    }
}
