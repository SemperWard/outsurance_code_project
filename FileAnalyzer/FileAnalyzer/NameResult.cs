﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAnalyzer
{
    public class NameResult
    {
        #region Constructors

        public NameResult()
        {
        }

        #endregion

        #region  Properties

        public string FirstName { get; set; }
        public  string Surname { get; set; }

        #endregion
    }
}
